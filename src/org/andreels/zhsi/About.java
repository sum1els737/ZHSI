/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zhsi;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class About extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String title;
	String user_first_name = "UNREGISTERED";
	String user_last_name = "UNREGISTERED";
	String thank_you_msg = "Please see Register link under the Help menu for more details";

	
	private final JPanel contentPanel = new JPanel();
	private Image logo_image = Toolkit.getDefaultToolkit().getImage(getClass().getResource("ZHSI_logo.png"));

	/**
	 * Create the dialog.
	 * @param title 
	 */
	public About(String title) {

	    if(ZHSIStatus.non_commercial_registered) {
	    	user_first_name = ZHSIStatus.user_name;
	    	user_last_name = ZHSIStatus.user_lastname;
	    	thank_you_msg = "";
	    }
		this.title = title;
		this.setIconImage(this.logo_image);
		this.setTitle("About ZHSI");
		setBounds(100, 100, 530, 530);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(15, 15, 15, 15));
		this.setLocationRelativeTo(getParent());
		this.setResizable(false);
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Close");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseReleased(MouseEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		{
			JLabel lblAbout = new JLabel();
			lblAbout.setHorizontalAlignment(SwingConstants.CENTER);
			getContentPane().add(lblAbout, BorderLayout.CENTER);
//			lblAbout.setText("<html><h2>" + this.title + "</h2> <h4>Author</h4> <p>Andre Els (aka m0ng00se @ x-plane.org)</p> "
//					+ "<h4>Credits</h4 "
//					+ "<p>Thanks to the XHSI Team for inspiration and some code snippets !</p>"
//					+ "<p>Thanks to Ville Ranki for his great ExtPlane Plugin</p>"
//					+ "<p></p></html>");
			lblAbout.setText("<html><div>\r\n" + 
					"<h2 style=\"text-align: center;\"><img src=\""
					+ About.class.getResource("ZHSI_logo.png")
					+ "\" alt=\"http://icons.iconarchive.com/icons/graphicloads/transport/128/airplane-takeoff-icon.png\" width=\"90\" height=\"90\" /></h2>\r\n" + 
					"<h2 style=\"text-align: center;\">" + this.title  + "</h2>\r\n" +
					"</div>\r\n" + 
					"<div style=\"text-align: center;\">\r\n" +
					"<h2><font color=\"RED\">FOR NON-COMMERCIAL HOME USE ONLY </h2>\r\n" +
					"<h3>Registered to:</h3>\r\n" + 
					"<p>" + user_first_name + " " + user_last_name + "</p>\r\n" +
					"<h3>" + thank_you_msg + "</h3>\r\n" +
					"</div>\r\n" +
					"<div style=\"text-align: left;\">\r\n" + 
					"<h4>Author</h4>\r\n" + 
					"</div>\r\n" + 
					"<div style=\"text-align: left;\">\r\n" + 
					"<p>Andre Els (aka m0ng00se @ x-plane.org)</p>\r\n" + 
					"</div>\r\n" + 
					"<div>\r\n" + 
					"<h4 style=\"text-align: left;\">Credits</h4>\r\n" + 
					"<p style=\"text-align: left;\">Thanks to the XHSI Team for inspiration !</p>\r\n" + 
					"<p style=\"text-align: left;\">Thanks to Ville Ranki for his great ExtPlane Plugin</p>\r\n" + 
					"</div></html>");
		}
	}

}
