/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zhsi.xpdata;

import org.andreels.zhsi.ExtPlaneInterface.ExtPlaneInterface;
import org.andreels.zhsi.ExtPlaneInterface.data.DataRef;
import org.andreels.zhsi.ExtPlaneInterface.util.Observer;

public class Radios extends BaseDataClass {
	
	Observer<DataRef> radios;

	private final String[] NAV_FREQ = {"sim/cockpit2/radios/actuators/nav1_frequency_hz", "sim/cockpit2/radios/actuators/nav2_frequency_hz"};
	private final String[] NAV_FREQ_STANDBY = {"sim/cockpit2/radios/actuators/nav1_standby_frequency_hz", "sim/cockpit2/radios/actuators/nav2_standby_frequency_hz"};
	private final String[] NAV_ID = {"sim/cockpit2/radios/indicators/nav1_nav_id:string", "sim/cockpit2/radios/indicators/nav2_nav_id:string"};
	private final String[] NAV_REL_BEARING = {"sim/cockpit2/radios/indicators/nav1_relative_bearing_deg", "sim/cockpit2/radios/indicators/nav2_relative_bearing_deg"};
	private final String[] NAV_DME_DIST_NM = {"sim/cockpit2/radios/indicators/nav1_dme_distance_nm", "sim/cockpit2/radios/indicators/nav2_dme_distance_nm"};
	private final String[] NAV_DME_TIME_SEC = {"sim/cockpit/radios/nav1_dme_time_secs","sim/cockpit/radios/nav2_dme_time_secs"};
	private final String[] NAV_FROMTO = {"sim/cockpit/radios/nav1_fromto", "sim/cockpit/radios/nav2_fromto2"};
	private final String[] NAV_HDEF_DOT = {"sim/cockpit2/radios/indicators/nav1_hdef_dots_pilot","sim/cockpit2/radios/indicators/nav2_hdef_dots_copilot"};
	private final String[] NAV_VDEF_DOT = {"sim/cockpit2/radios/indicators/nav1_vdef_dots_pilot", "sim/cockpit2/radios/indicators/nav2_vdef_dots_copilot"};
	private final String[] NAV_CDI = {"sim/cockpit/radios/nav1_CDI", "sim/cockpit/radios/nav2_CDI"};
	private final String[] ADF_FREQ = {"sim/cockpit2/radios/actuators/adf1_frequency_hz", "sim/cockpit2/radios/actuators/adf2_frequency_hz"};
	private final String[] ADF_ID = {"sim/cockpit2/radios/indicators/adf1_nav_id:string", "sim/cockpit2/radios/indicators/adf2_nav_id:string"};
	private final String[] ADF_REL_BREARING = {"sim/cockpit2/radios/indicators/adf1_relative_bearing_deg", "sim/cockpit2/radios/indicators/adf2_relative_bearing_deg"};
	private final String[] ADF_DME_DIST_NM = {"sim/cockpit2/radios/indicators/adf1_dme_distance_nm", "sim/cockpit2/radios/indicators/adf2_dme_distance_nm"};
	private final String[] NAV_COURSE = {"sim/cockpit/radios/nav1_course_degm", "sim/cockpit/radios/nav2_course_degm"};
	private final String NAV_TYPE = "sim/cockpit/radios/nav_type";
	private final String RMI_ARROW1 = "laminar/B738/radio/arrow1";
	private final String RMI_ARROW2 = "laminar/B738/radio/arrow2";
	private final String RMI_ARROW1_NO_AVAIL = "laminar/B738/radio/arrow1_no_available";
	private final String RMI_ARROW2_NO_AVAIL = "laminar/B738/radio/arrow2_no_available";

	public int[] nav_freq = new int[2];
	public int[] nav_freq_standby = new int[2];
	public String[] nav_id = new String[2];
	public float[] nav_rel_bearing = new float[2];
	public float[] nav_dme_dist_nm = new float[2];
	public float[] nav_dme_time_sec = new float[2];
	public int[] nav_fromto = new int[2];
	public float[] nav_hdef_dot = new float[2];
	public float[] nav_vdef_dot = new float[2];
	public int[] nav_cdi = new int[2];
	public int[] adf_freq = new int[2];
	public String[] adf_id = new String[2];
	public float[] adf_rel_brearing = new float[2];
	public float[] adf_dme_dist_nm = new float[2];
	public float[] nav_course = new float[2];
	public int[] nav_type = new int[6];
	public float rmi_arrow1 = 0f;
	public float rmi_arrow2 = 0f;
	public float rmi_arrow1_no_avail = 0f;
	public float rmi_arrow2_no_avail = 0f;

	public Radios(ExtPlaneInterface iface) {
		super(iface);	
		nav_freq[0] = 0;
		
		
		radios = new Observer<DataRef>() {

			@Override
			public void update(DataRef object) {
				
				if (object.getName().equals(RMI_ARROW1)) {
					rmi_arrow1 = Float.parseFloat(object.getValue()[0]);
				}
				if (object.getName().equals(RMI_ARROW2)) {
					rmi_arrow2 = Float.parseFloat(object.getValue()[0]);
				}
				if (object.getName().equals(RMI_ARROW1_NO_AVAIL)) {
					rmi_arrow1_no_avail = Float.parseFloat(object.getValue()[0]);
				}
				if (object.getName().equals(RMI_ARROW2_NO_AVAIL)) {
					rmi_arrow2_no_avail = Float.parseFloat(object.getValue()[0]);
				}
				
				for (int i = 0; i < 2; i++) {
					if(object.getName().equals(NAV_FREQ[i])) {
						nav_freq[i] = Integer.parseInt(object.getValue()[0]);					
					}
					if(object.getName().equals(NAV_FREQ_STANDBY[i])) {
						nav_freq_standby[i] = Integer.parseInt(object.getValue()[0]);					
					}
					if(object.getName().equals(NAV_ID[i])) {
						nav_id[i] = object.getValue()[0].replaceAll("\"", "");			
					}
					if(object.getName().equals(NAV_REL_BEARING[i])) {
						nav_rel_bearing[i] = Float.parseFloat(object.getValue()[0]);					
					}
					if(object.getName().equals(NAV_DME_DIST_NM[i])) {
						nav_dme_dist_nm[i] = Float.parseFloat(object.getValue()[0]);					
					}
					if(object.getName().equals(NAV_DME_TIME_SEC[i])) {
						nav_dme_time_sec[i] = Float.parseFloat(object.getValue()[0]);					
					}
					if(object.getName().equals(NAV_FROMTO[i])) {
						nav_fromto[i] = Integer.parseInt(object.getValue()[0]);					
					}
					if(object.getName().equals(NAV_HDEF_DOT[i])) {
						nav_hdef_dot[i] = Float.parseFloat(object.getValue()[0]);					
					}
					if(object.getName().equals(NAV_VDEF_DOT[i])) {
						nav_vdef_dot[i] = Float.parseFloat(object.getValue()[0]);					
					}
					if(object.getName().equals(NAV_CDI[i])) {
						nav_cdi[i] = Integer.parseInt(object.getValue()[0]);					
					}
					if(object.getName().equals(ADF_FREQ[i])) {
						adf_freq[i] = Integer.parseInt(object.getValue()[0]);					
					}
					if(object.getName().equals(ADF_ID[i])) {
						adf_id[i] = object.getValue()[0].replaceAll("\"", "");				
					}
					if(object.getName().equals(ADF_REL_BREARING[i])) {
						adf_rel_brearing[i] = Float.parseFloat(object.getValue()[0]);					
					}
					if(object.getName().equals(ADF_DME_DIST_NM[i])) {
						adf_dme_dist_nm[i] = Float.parseFloat(object.getValue()[0]);					
					}
					if(object.getName().equals(NAV_COURSE[i])) {
						nav_course[i] = Float.parseFloat(object.getValue()[0]);					
					}
				}
				if (object.getName().equals(NAV_TYPE)) {
					for(int i = 0; i < 6; i++) {
						nav_type[i] = Integer.parseInt(object.getValue()[i]);
					}
				}
			}
		};
	}

	@Override
	public void subscribeDrefs() {

	}

	@Override
	public void includeDrefs() {
		
		iface.includeDataRef(NAV_TYPE);
		iface.observeDataRef(NAV_TYPE, radios);
		iface.includeDataRef(RMI_ARROW1, 0.01f);
		iface.observeDataRef(RMI_ARROW1, radios);
		iface.includeDataRef(RMI_ARROW2, 0.01f);
		iface.observeDataRef(RMI_ARROW2, radios);
		iface.includeDataRef(RMI_ARROW1_NO_AVAIL, 0.01f);
		iface.observeDataRef(RMI_ARROW1_NO_AVAIL, radios);
		iface.includeDataRef(RMI_ARROW2_NO_AVAIL, 0.01f);
		iface.observeDataRef(RMI_ARROW2_NO_AVAIL, radios);
		
		for (int i = 0; i < 2; i++) {
			iface.includeDataRef(NAV_FREQ[i]);
			iface.includeDataRef(NAV_FREQ_STANDBY[i]);
			iface.includeDataRef(NAV_ID[i]);
			iface.includeDataRef(NAV_REL_BEARING[i], 0.001f);
			iface.includeDataRef(NAV_DME_DIST_NM[i], 0.01f);
			iface.includeDataRef(NAV_DME_TIME_SEC[i]);
			iface.includeDataRef(NAV_FROMTO[i]);
			iface.includeDataRef(NAV_HDEF_DOT[i]);
			iface.includeDataRef(NAV_VDEF_DOT[i]);
			iface.includeDataRef(NAV_CDI[i]);
			iface.includeDataRef(ADF_FREQ[i]);
			iface.includeDataRef(ADF_ID[i]);
			iface.includeDataRef(ADF_REL_BREARING[i], 0.001f);
			iface.includeDataRef(ADF_DME_DIST_NM[i], 0.01f);
			iface.includeDataRef(NAV_COURSE[i], 0.1f);
			
			iface.observeDataRef(NAV_FREQ[i], radios);
			iface.observeDataRef(NAV_FREQ_STANDBY[i], radios);
			iface.observeDataRef(NAV_ID[i], radios);
			iface.observeDataRef(NAV_REL_BEARING[i], radios);
			iface.observeDataRef(NAV_DME_DIST_NM[i], radios);
			iface.observeDataRef(NAV_DME_TIME_SEC[i], radios);
			iface.observeDataRef(NAV_FROMTO[i], radios);
			iface.observeDataRef(NAV_HDEF_DOT[i], radios);
			iface.observeDataRef(NAV_VDEF_DOT[i], radios);
			iface.observeDataRef(NAV_CDI[i], radios);
			iface.observeDataRef(ADF_FREQ[i], radios);
			iface.observeDataRef(ADF_ID[i], radios);
			iface.observeDataRef(ADF_REL_BREARING[i], radios);
			iface.observeDataRef(ADF_DME_DIST_NM[i], radios);
			iface.observeDataRef(NAV_COURSE[i], radios);
		}
		
	}

	@Override
	public void excludeDrefs() {
		
		iface.excludeDataRef(NAV_TYPE);
		iface.unObserveDataRef(NAV_TYPE, radios);
		iface.excludeDataRef(RMI_ARROW1);
		iface.unObserveDataRef(RMI_ARROW1, radios);
		iface.excludeDataRef(RMI_ARROW2);
		iface.unObserveDataRef(RMI_ARROW2, radios);
		iface.excludeDataRef(RMI_ARROW1_NO_AVAIL);
		iface.unObserveDataRef(RMI_ARROW1_NO_AVAIL, radios);
		iface.excludeDataRef(RMI_ARROW2_NO_AVAIL);
		iface.unObserveDataRef(RMI_ARROW2_NO_AVAIL, radios);
		
		for (int i = 0; i < 2; i++) {
			iface.excludeDataRef(NAV_FREQ[i]);
			iface.excludeDataRef(NAV_FREQ_STANDBY[i]);
			iface.excludeDataRef(NAV_ID[i]);
			iface.excludeDataRef(NAV_REL_BEARING[i]);
			iface.excludeDataRef(NAV_DME_DIST_NM[i]);
			iface.excludeDataRef(NAV_DME_TIME_SEC[i]);
			iface.excludeDataRef(NAV_FROMTO[i]);
			iface.excludeDataRef(NAV_HDEF_DOT[i]);
			iface.excludeDataRef(NAV_VDEF_DOT[i]);
			iface.excludeDataRef(NAV_CDI[i]);
			iface.excludeDataRef(ADF_FREQ[i]);
			iface.excludeDataRef(ADF_ID[i]);
			iface.excludeDataRef(ADF_REL_BREARING[i]);
			iface.excludeDataRef(ADF_DME_DIST_NM[i]);
			iface.excludeDataRef(NAV_COURSE[i]);
			
			iface.unObserveDataRef(NAV_FREQ[i], radios);
			iface.unObserveDataRef(NAV_FREQ_STANDBY[i], radios);
			iface.unObserveDataRef(NAV_ID[i], radios);
			iface.unObserveDataRef(NAV_REL_BEARING[i], radios);
			iface.unObserveDataRef(NAV_DME_DIST_NM[i], radios);
			iface.unObserveDataRef(NAV_DME_TIME_SEC[i], radios);
			iface.unObserveDataRef(NAV_FROMTO[i], radios);
			iface.unObserveDataRef(NAV_HDEF_DOT[i], radios);
			iface.unObserveDataRef(NAV_VDEF_DOT[i], radios);
			iface.unObserveDataRef(NAV_CDI[i], radios);
			iface.unObserveDataRef(ADF_FREQ[i], radios);
			iface.unObserveDataRef(ADF_ID[i], radios);
			iface.unObserveDataRef(ADF_REL_BREARING[i], radios);
			iface.unObserveDataRef(ADF_DME_DIST_NM[i], radios);
			iface.unObserveDataRef(NAV_COURSE[i], radios);
		}
		
	}

}
