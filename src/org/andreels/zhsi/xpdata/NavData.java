/**
 * 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zhsi.xpdata;

import java.text.DecimalFormat;

import org.andreels.zhsi.ExtPlaneInterface.ExtPlaneInterface;
import org.andreels.zhsi.ExtPlaneInterface.data.DataRef;
import org.andreels.zhsi.ExtPlaneInterface.util.Observer;

public class NavData extends BaseDataClass {
	
	Observer<DataRef> tc_td_decel;
	Observer<DataRef> fixes;
	Observer<DataRef> runways;
	Observer<DataRef> irs;

	private DecimalFormat df3 = new DecimalFormat("000");


//	@SuppressWarnings("unused")
//	private final String[] APT_X = {"laminar/B738/nd/apt_x", "laminar/B738/nd/apt_fo_x"};
//	@SuppressWarnings("unused")
//	private final String[] APT_Y = {"laminar/B738/nd/apt_y", "laminar/B738/nd/apt_fo_y"};
//	@SuppressWarnings("unused")
//	private final String[] APT_ENABLE = {"laminar/B738/nd/apt_enable", "laminar/B738/nd/apt_fo_enable"};
//	@SuppressWarnings("unused")
//	private final String[] APT_ID = {
//			"laminar/B738/nd/apt_id00:string",
//			"laminar/B738/nd/apt_id01:string",
//			"laminar/B738/nd/apt_id02:string",
//			"laminar/B738/nd/apt_id03:string",
//			"laminar/B738/nd/apt_id04:string",
//			"laminar/B738/nd/apt_id05:string",
//			"laminar/B738/nd/apt_id06:string",
//			"laminar/B738/nd/apt_id07:string",
//			"laminar/B738/nd/apt_id08:string",
//			"laminar/B738/nd/apt_id09:string",
//			"laminar/B738/nd/apt_id10:string",
//			"laminar/B738/nd/apt_id11:string",
//			"laminar/B738/nd/apt_id12:string",
//			"laminar/B738/nd/apt_id13:string",
//			"laminar/B738/nd/apt_id14:string",
//			"laminar/B738/nd/apt_id15:string",
//			"laminar/B738/nd/apt_id16:string",
//			"laminar/B738/nd/apt_id17:string",
//			"laminar/B738/nd/apt_id18:string",
//			"laminar/B738/nd/apt_id19:string",
//			"laminar/B738/nd/apt_id20:string",
//			"laminar/B738/nd/apt_id21:string",
//			"laminar/B738/nd/apt_id22:string",
//			"laminar/B738/nd/apt_id23:string",
//			"laminar/B738/nd/apt_id24:string",
//			"laminar/B738/nd/apt_id25:string",
//			"laminar/B738/nd/apt_id26:string",
//			"laminar/B738/nd/apt_id27:string",
//			"laminar/B738/nd/apt_id28:string",
//			"laminar/B738/nd/apt_id29:string"
//	};
//	@SuppressWarnings("unused")
//	private final String[] APT_ID_FO = {
//			"laminar/B738/nd/apt_fo_id00:string",
//			"laminar/B738/nd/apt_fo_id01:string",
//			"laminar/B738/nd/apt_fo_id02:string",
//			"laminar/B738/nd/apt_fo_id03:string",
//			"laminar/B738/nd/apt_fo_id04:string",
//			"laminar/B738/nd/apt_fo_id05:string",
//			"laminar/B738/nd/apt_fo_id06:string",
//			"laminar/B738/nd/apt_fo_id07:string",
//			"laminar/B738/nd/apt_fo_id08:string",
//			"laminar/B738/nd/apt_fo_id09:string",
//			"laminar/B738/nd/apt_fo_id10:string",
//			"laminar/B738/nd/apt_fo_id11:string",
//			"laminar/B738/nd/apt_fo_id12:string",
//			"laminar/B738/nd/apt_fo_id13:string",
//			"laminar/B738/nd/apt_fo_id14:string",
//			"laminar/B738/nd/apt_fo_id15:string",
//			"laminar/B738/nd/apt_fo_id16:string",
//			"laminar/B738/nd/apt_fo_id17:string",
//			"laminar/B738/nd/apt_fo_id18:string",
//			"laminar/B738/nd/apt_fo_id19:string",
//			"laminar/B738/nd/apt_fo_id20:string",
//			"laminar/B738/nd/apt_fo_id21:string",
//			"laminar/B738/nd/apt_fo_id22:string",
//			"laminar/B738/nd/apt_fo_id23:string",
//			"laminar/B738/nd/apt_fo_id24:string",
//			"laminar/B738/nd/apt_fo_id25:string",
//			"laminar/B738/nd/apt_fo_id26:string",
//			"laminar/B738/nd/apt_fo_id27:string",
//			"laminar/B738/nd/apt_fo_id28:string",
//			"laminar/B738/nd/apt_fo_id29:string"
//	};
	
	private final String[] TC_ID = {"laminar/B738/nd/tc_id:string", "laminar/B738/nd/tc_fo_id:string"};
	private final String[] TC_SHOW = {"laminar/B738/nd/tc_show", "laminar/B738/nd/tc_fo_show"};
	private final String[] TC_X = {"laminar/B738/nd/tc_x", "laminar/B738/nd/tc_fo_x"};
	private final String[] TC_Y = {"laminar/B738/nd/tc_y", "laminar/B738/nd/tc_fo_y"};
	
	private final String[] DECEL_ID = {"laminar/B738/nd/decel_id:string", "laminar/B738/nd/decel_fo_id:string"};
	private final String[] DECEL_SHOW = {"laminar/B738/nd/decel_show", "laminar/B738/nd/decel_fo_show"};
	private final String[] DECEL_X = {"laminar/B738/nd/decel_x", "laminar/B738/nd/decel_fo_x"};
	private final String[] DECEL_Y = {"laminar/B738/nd/decel_y", "laminar/B738/nd/decel_fo_y"};
	
	private final String[] TD_ID = {"laminar/B738/nd/td_id:string", "laminar/B738/nd/td_fo_id:string"};
	private final String[] TD_SHOW = {"laminar/B738/nd/td_show", "laminar/B738/nd/td_fo_show"};
	private final String[] TD_X = {"laminar/B738/nd/td_x", "laminar/B738/nd/td_fo_x"};
	private final String[] TD_Y = {"laminar/B738/nd/td_y", "laminar/B738/nd/td_fo_y"};
	
	private final String ILS_X = "laminar/B738/pfd/ils_x";
	private final String ILS_Y = "laminar/B738/pfd/ils_y";	
	private final String ILS_X0 = "laminar/B738/pfd/ils_x0";
	private final String ILS_Y0 = "laminar/B738/pfd/ils_y0";
	
	private final String ILS_FO_X = "laminar/B738/pfd/ils_fo_x";
	private final String ILS_FO_Y = "laminar/B738/pfd/ils_fo_y";	
	private final String ILS_FO_X0 = "laminar/B738/pfd/ils_fo_x0";
	private final String ILS_FO_Y0 = "laminar/B738/pfd/ils_fo_y0";
	
	private final String ILS_RUNWAY = "laminar/B738/pfd/ils_runway:string";
	private final String ILS_RUNWAY0 = "laminar/B738/pfd/ils_runway0:string";

	

	public float[][] apt_x = new float[2][30];
	public float[][] apt_y = new float[2][30];
	public String[] apt_id = new String[30];
	public String[] apt_fo_id = new String[30];
	public int[][] apt_enable = new int[2][30];
	//
	public int[] tc_show = new int[2];
	public String[] tc_id = new String[2];
	public float[] tc_x = new float[2];
	public float[] tc_y = new float[2];
	//
	public int[] decel_show = new int[2];
	public String[] decel_id = new String[2];
	public float[] decel_x = new float[2];
	public float[] decel_y = new float[2];
	//
	public int[] td_show = new int[2];
	public String[] td_id = new String[2];
	public float[] td_x = new float[2];
	public float[] td_y = new float[2];
	

	public float ils_x = 0.0f;
	public float ils_y = 0.0f;
	public float ils_x0 = 0.0f;
	public float ils_y0 = 0.0f;
	public float ils_fo_x = 0.0f;
	public float ils_fo_y = 0.0f;
	public float ils_fo_x0 = 0.0f;
	public float ils_fo_y0 = 0.0f;
	
	public String ils_runway = "";
	public String ils_runway0 = "";

	
	
	private final String FIX_ID0 = "laminar/B738/nd/fix_id00:string";
	private final String FIX_ID1 = "laminar/B738/nd/fix_id01:string";
	private final String FIX_ID2 = "laminar/B738/nd/fix_id02:string";
	private final String FIX_ID3 = "laminar/B738/nd/fix_id03:string";
	private final String FIX_ID4 = "laminar/B738/nd/fix_id04:string";
	
	private final String FIX_FO_ID0 = "laminar/B738/nd/fix_fo_id00:string";
	private final String FIX_FO_ID1 = "laminar/B738/nd/fix_fo_id01:string";
	private final String FIX_FO_ID2 = "laminar/B738/nd/fix_fo_id02:string";
	private final String FIX_FO_ID3 = "laminar/B738/nd/fix_fo_id03:string";
	private final String FIX_FO_ID4 = "laminar/B738/nd/fix_fo_id04:string";
	
	private final String FIX_DIST_0 = "laminar/B738/nd/fix_dist_0_nm";
	private final String FIX_DIST_1 = "laminar/B738/nd/fix_dist_1_nm";
	private final String FIX_DIST_2 = "laminar/B738/nd/fix_dist_2_nm";
	
	private final String FIX_RAD_DIST_0 = "laminar/B738/nd/fix_rad_dist_0";
	private final String FIX_RAD_DIST_1 = "laminar/B738/nd/fix_rad_dist_1";
	private final String FIX_RAD_DIST_2 = "laminar/B738/nd/fix_rad_dist_2";
	
	private final String[] FIX_ID_SHOW = {"laminar/B738/nd/fix_show", "laminar/B738/nd/fix_fo_show"};
	
	public float[] fix_dist_0 = new float[5];
	public float[] fix_dist_1 = new float[5];
	public float[] fix_dist_2 = new float[5];
	
	public float[] fix_rad_dist_0 = new float[5];
	public float[] fix_rad_dist_1 = new float[5];
	public float[] fix_rad_dist_2 = new float[5];
	
	public boolean[] fix_id_show = new boolean[5];
	public boolean[] fix_id_fo_show = new boolean[5];
	public String[] fix_id = new String[5];
	public String[] fix_fo_id = new String[5];
	
	private final String IRS_DECIMALS_SHOW = "laminar/B738/decimals_show";
	private final String IRS_NS_SHOW = "laminar/B738/ns_show";
	private final String IRS_EW_SHOW = "laminar/B738/ew_show";
	private final String IRS_LEFT1_SHOW = "laminar/B738/irs_left1_show";
	private final String IRS_LEFT2_SHOW = "laminar/B738/irs_left2_show";
	private final String IRS_RIGHT1_SHOW = "laminar/B738/irs_right1_show";
	private final String IRS_RIGHT2_SHOW = "laminar/B738/irs_right2_show";
	private final String IRS_ALIGN_SHOW = "laminar/B738/irs_allign_show";
	private final String IRS_LAT_DEG_SHOW = "laminar/B738/lat_deg_show";
	private final String IRS_LAT_MIN_SHOW = "laminar/B738/lat_min_show";
	private final String IRS_LON_DEG_SHOW = "laminar/B738/lon_deg_show";
	private final String IRS_LON_MIN_SHOW = "laminar/B738/lon_min_show";
	private final String IRS_EW = "laminar/B738/longitude_EW";
	private final String IRS_NS = "laminar/B738/latitude_NS";
	//
	private final String IRS_LEFT1 = "laminar/B738/irs_left1";
	private final String IRS_LEFT2 = "laminar/B738/irs_left2";
	private final String IRS_RIGHT1 = "laminar/B738/irs_right1";
	private final String IRS_RIGHT2 = "laminar/B738/irs_right2";
	//
	private final String IRS_LAT_DEG = "laminar/B738/latitude_deg";
	private final String IRS_LAT_MIN = "laminar/B738/latitude_min";
	private final String IRS_LON_DEG = "laminar/B738/longitude_deg";
	private final String IRS_LON_MIN = "laminar/B738/longitude_min";
	//
	private final String IRS_KNOB = "laminar/B738/toggle_switch/irs_dspl_sel";
	private final String IRS_LEFT_REMAIN = "laminar/B738/irs/alignment_left_remain";
	private final String IRS_RIGHT_REMAIN = "laminar/B738/irs/alignment_right_remain";
	
	
	
	public String irs_left_string = "";
	public String irs_right_string = "";
	public float irs_decimals_show = 0;
	private float irs_lat_deg_show = 0;
	private float irs_lat_min_show = 0;
	private float irs_lon_deg_show = 0;
	private float irs_lon_min_show = 0;
	private float irs_ns = 0;
	private float irs_ew = 0;
	private float irs_lat_deg = 0;
	private float irs_lat_min = 0;
	private float irs_lon_deg = 0;
	private float irs_lon_min = 0;
	private float irs_knob = 0;
	private float irs_ns_show = 0;
	private float irs_ew_show = 0;
	private float irs_left2 = 0;
	private float irs_right2 = 0;
	private float irs_left1 = 0;
	private float irs_right1 = 0;
	private float irs_left2_show = 0;
	private float irs_right2_show = 0;
	private float irs_left1_show = 0;
	private float irs_right1_show = 0;
	private float irs_left_remain = 0;
	private float irs_right_remain = 0;
	
	


	public NavData(ExtPlaneInterface iface) {
		super(iface);
		
		//only for irs
		this.drefs.add(IRS_DECIMALS_SHOW);
		this.drefs.add(IRS_NS_SHOW);
		this.drefs.add(IRS_EW_SHOW);
		this.drefs.add(IRS_LEFT1_SHOW);
		this.drefs.add(IRS_LEFT2_SHOW);
		this.drefs.add(IRS_RIGHT1_SHOW);
		this.drefs.add(IRS_RIGHT2_SHOW);
		this.drefs.add(IRS_ALIGN_SHOW);
		this.drefs.add(IRS_LAT_DEG_SHOW);
		this.drefs.add(IRS_LAT_MIN_SHOW);
		this.drefs.add(IRS_LON_DEG_SHOW);
		this.drefs.add(IRS_LON_MIN_SHOW);
		this.drefs.add(IRS_LEFT1);
		this.drefs.add(IRS_LEFT2);
		this.drefs.add(IRS_RIGHT1);
		this.drefs.add(IRS_RIGHT2);
		this.drefs.add(IRS_EW);
		this.drefs.add(IRS_NS);
		this.drefs.add(IRS_LAT_DEG);
		this.drefs.add(IRS_LAT_MIN);
		this.drefs.add(IRS_LON_DEG);
		this.drefs.add(IRS_LON_MIN);
		this.drefs.add(IRS_KNOB);
		this.drefs.add(IRS_LEFT_REMAIN);
		this.drefs.add(IRS_RIGHT_REMAIN);
		
		irs = new Observer<DataRef>() {

			@Override
			public void update(DataRef object) {
				
				switch(object.getName()) {
				case IRS_NS_SHOW: irs_ns_show = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_EW_SHOW: irs_ew_show = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_DECIMALS_SHOW: irs_decimals_show = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_NS: irs_ns = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_EW: irs_ew = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_LAT_DEG_SHOW: irs_lat_deg_show = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_LAT_MIN_SHOW: irs_lon_min_show = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_LON_DEG_SHOW: irs_lon_deg_show = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_LON_MIN_SHOW: irs_lat_min_show = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_LAT_DEG: irs_lat_deg = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_LAT_MIN: irs_lat_min = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_LON_DEG: irs_lon_deg = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_LON_MIN: irs_lon_min = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_LEFT2: irs_left2 = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_RIGHT2: irs_right2 = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_LEFT1: irs_left1 = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_RIGHT1: irs_right1 = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_LEFT2_SHOW: irs_left2_show = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_RIGHT2_SHOW: irs_right2_show = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_LEFT1_SHOW: irs_left1_show = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_RIGHT1_SHOW: irs_right1_show = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_LEFT_REMAIN: irs_left_remain = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_RIGHT_REMAIN: irs_right_remain = Float.parseFloat(object.getValue()[0]);
				break;
				case IRS_KNOB: irs_knob = Float.parseFloat(object.getValue()[0]);
				break;
				}
				buildIrsStrings();
			}

			private void buildIrsStrings() {
				
				boolean irs_aligned = false;
				
				irs_aligned = (irs_left_remain == 0 || irs_right_remain == 0);
				
				
				if(irs_ew_show == 2 || irs_ns_show == 2) { //test
					irs_left_string = "~88888";
					irs_right_string = "~188888";
				}else {
					if(irs_knob == 1) { //TK/GS //TEST
						if(!irs_aligned) {
							irs_left_string = "";
							irs_right_string = "";
						}else {
							irs_left_string = df3.format(irs_left2).replaceAll("0", "O");
							irs_right_string = df3.format(irs_right2).replaceAll("0", "O");
						}
					}else if(irs_knob == 2) { //PPOS
						if(!irs_aligned) {
							irs_left_string = "";
							irs_right_string = "";
						}else {
							if(irs_ns == 0) {
								irs_left_string = ("N" + (int)irs_lat_deg).replaceAll("0", "O") + ("" + (int)irs_lat_min).replaceAll("0", "O");
							}else {
								irs_left_string = ("S" + (int)irs_lat_deg).replaceAll("0", "O") + ("" + (int)irs_lat_min).replaceAll("0", "O");
							}
							if(irs_ew == 0) {
								irs_right_string = "E" + df3.format(irs_lon_deg).replaceAll("0", "O") + ("" + (int)irs_lon_min).replaceAll("0", "O");
							}else {
								irs_right_string = "W" + df3.format(irs_lon_deg).replaceAll("0", "O") + ("" + (int)irs_lon_min).replaceAll("0", "O");
							}
						}

					}else if(irs_knob == 3) { //WIND
						if(!irs_aligned) {
							irs_left_string = "";
							irs_right_string = "";
						}else {
							irs_left_string = df3.format(irs_left2).replaceAll("0", "O");
							irs_right_string = df3.format(irs_right2).replaceAll("0", "O");
						}
					}else if (irs_knob == 4) { //HDG/STS
						if(!irs_aligned) {
							irs_left_string = "";
							irs_right_string = "" + (int)irs_right1 + "                    ";
						}else {
							if(irs_left1_show == 1) {
								irs_left_string = df3.format(irs_left1).replaceAll("0", "O");
							}
							if(irs_left2_show == 1){
								irs_left_string = df3.format(irs_left2).replaceAll("0", "O");
							}
							if(irs_right1_show == 1) {
								irs_right_string = df3.format(irs_right1).replaceAll("0", "O");
							}
							if(irs_right2_show == 1) {
								irs_right_string = df3.format(irs_right2).replaceAll("0", "O");
							}
						}

					}else {
						irs_left_string = "";
						irs_right_string = "";
					}
				}
			}
			
		};
		
		tc_td_decel = new Observer<DataRef>() {

			@Override
			public void update(DataRef object) {
				for(int i = 0; i < 2; i++) {
					
					if(object.getName().equals(TC_ID[i])) {
						tc_id[i] = object.getValue()[0].replaceAll("\"", "");	
					}
					if(object.getName().equals(TC_SHOW[i])) {
						tc_show[i] = Integer.parseInt(object.getValue()[0]);
					}
					if(object.getName().equals(TC_X[i])) {
						tc_x[i] = Float.parseFloat(object.getValue()[0]);
					}
					if(object.getName().equals(TC_Y[i])) {
						tc_y[i] = Float.parseFloat(object.getValue()[0]);
					}
					if(object.getName().equals(DECEL_ID[i])) {
						decel_id[i] = object.getValue()[0].replaceAll("\"", "");
					}
					if(object.getName().equals(DECEL_SHOW[i])) {
						decel_show[i] = Integer.parseInt(object.getValue()[0]);
					}
					if(object.getName().equals(DECEL_X[i])) {
						decel_x[i] = Float.parseFloat(object.getValue()[0]);
					}
					if(object.getName().equals(DECEL_Y[i])) {
						decel_y[i] = Float.parseFloat(object.getValue()[0]);
					}
					if(object.getName().equals(TD_ID[i])) {
						td_id[i] = object.getValue()[0].replaceAll("\"", "");
					}
					if(object.getName().equals(TD_SHOW[i])) {
						td_show[i] = Integer.parseInt(object.getValue()[0]);
					}
					if(object.getName().equals(TD_X[i])) {
						td_x[i] = Float.parseFloat(object.getValue()[0]);
					}
					if(object.getName().equals(TD_Y[i])) {
						td_y[i] = Float.parseFloat(object.getValue()[0]);
					}
				}
			}
			
		};
		
		fixes = new Observer<DataRef>() {

			@Override
			public void update(DataRef object) {
				
				if(object.getName().equals(FIX_ID_SHOW[0])) {
					for(int i = 0; i < 5; i++) {
						int val = Integer.parseInt(object.getValue()[i]);
						if(val == 1) {
							fix_id_show[i] = true;
						}else {
							fix_id_show[i] = false;
						}
					}
				}
				
				switch(object.getName()) {
		
				
				case FIX_ID0: fix_id[0] = object.getValue()[0].replaceAll("\"", "");
				break;
				case FIX_ID1: fix_id[1] = object.getValue()[0].replaceAll("\"", "");
				break;
				case FIX_ID2: fix_id[2] = object.getValue()[0].replaceAll("\"", "");
				break;
				case FIX_ID3: fix_id[3] = object.getValue()[0].replaceAll("\"", "");
				break;
				case FIX_ID4: fix_id[4] = object.getValue()[0].replaceAll("\"", "");
				break;
				case FIX_FO_ID0: fix_fo_id[0] = object.getValue()[0].replaceAll("\"", "");
				break;
				case FIX_FO_ID1: fix_fo_id[1] = object.getValue()[0].replaceAll("\"", "");
				break;
				case FIX_FO_ID2: fix_fo_id[2] = object.getValue()[0].replaceAll("\"", "");
				break;
				case FIX_FO_ID3: fix_fo_id[3] = object.getValue()[0].replaceAll("\"", "");
				break;
				case FIX_FO_ID4: fix_fo_id[4] = object.getValue()[0].replaceAll("\"", "");
				break;
				}
				
				
				
				if(object.getName().equals(FIX_DIST_0)) {
					for(int i = 0; i < 5; i++) {
						fix_dist_0[i] = Float.parseFloat(object.getValue()[i]);
					}
				}
				if(object.getName().equals(FIX_DIST_1)) {
					for(int i = 0; i < 5; i++) {
						fix_dist_1[i] = Float.parseFloat(object.getValue()[i]);
					}
				}
				if(object.getName().equals(FIX_DIST_2)) {
					for(int i = 0; i < 5; i++) {
						fix_dist_2[i] = Float.parseFloat(object.getValue()[i]);
					}
				}
				if(object.getName().equals(FIX_RAD_DIST_0)) {
					for(int i = 0; i < 5; i++) {
						fix_rad_dist_0[i] = Float.parseFloat(object.getValue()[i]);
					}
				}
				if(object.getName().equals(FIX_RAD_DIST_1)) {
					for(int i = 0; i < 5; i++) {
						fix_rad_dist_1[i] = Float.parseFloat(object.getValue()[i]);
					}
				}
				if(object.getName().equals(FIX_RAD_DIST_2)) {
					for(int i = 0; i < 5; i++) {
						fix_rad_dist_2[i] = Float.parseFloat(object.getValue()[i]);
					}
				}
				
				if(object.getName().equals(FIX_ID_SHOW[1])) {
					for(int i = 0; i < 5; i++) {
						int val = Integer.parseInt(object.getValue()[i]);
						if(val == 1) {
							fix_id_fo_show[i] = true;
						}else {
							fix_id_fo_show[i] = false;
						}
					}
				}
			}
			
		};
		
		runways = new Observer<DataRef>() {

			@Override
			public void update(DataRef object) {

				switch(object.getName()) {

				case ILS_X: ils_x = Float.parseFloat(object.getValue()[0]);
				break;
				case ILS_Y: ils_y = Float.parseFloat(object.getValue()[0]);
				break;
				case ILS_X0: ils_x0 = Float.parseFloat(object.getValue()[0]);
				break;
				case ILS_Y0: ils_y0 = Float.parseFloat(object.getValue()[0]);
				break;
				case ILS_FO_X: ils_fo_x = Float.parseFloat(object.getValue()[0]);
				break;
				case ILS_FO_Y: ils_fo_y = Float.parseFloat(object.getValue()[0]);
				break;
				case ILS_FO_X0: ils_fo_x0 = Float.parseFloat(object.getValue()[0]);
				break;
				case ILS_FO_Y0: ils_fo_y0 = Float.parseFloat(object.getValue()[0]);
				break;
				case ILS_RUNWAY: ils_runway = object.getValue()[0].replaceAll("\"", "");
				break;
				case ILS_RUNWAY0: ils_runway0 = object.getValue()[0].replaceAll("\"", "");
				break;
				}
				
			}
			
		};
	}

	
	
	
	@Override
	public void subscribeDrefs() {
		



		
	}


	@Override
	public void includeDrefs() {
		
		//irs
		for(String dref : drefs) {
			iface.includeDataRef(dref);
			iface.observeDataRef(dref, irs);
		}
	
		for(int i = 0; i < 2; i++) {
			iface.includeDataRef(TC_ID[i]);
			iface.includeDataRef(TC_SHOW[i]);
			iface.includeDataRef(TC_X[i], 0.001f);
			iface.includeDataRef(TC_Y[i], 0.001f);
			//
			iface.includeDataRef(DECEL_ID[i]);
			iface.includeDataRef(DECEL_SHOW[i]);
			iface.includeDataRef(DECEL_X[i], 0.001f);
			iface.includeDataRef(DECEL_Y[i], 0.001f);
			//
			iface.includeDataRef(TD_ID[i]);
			iface.includeDataRef(TD_SHOW[i]);
			iface.includeDataRef(TD_X[i], 0.001f);
			iface.includeDataRef(TD_Y[i], 0.001f);
			//
			iface.observeDataRef(TC_ID[i], tc_td_decel);
			iface.observeDataRef(TC_SHOW[i], tc_td_decel);
			iface.observeDataRef(TC_X[i], tc_td_decel);
			iface.observeDataRef(TC_Y[i], tc_td_decel);
			//
			iface.observeDataRef(DECEL_ID[i], tc_td_decel);
			iface.observeDataRef(DECEL_SHOW[i], tc_td_decel);
			iface.observeDataRef(DECEL_X[i], tc_td_decel);
			iface.observeDataRef(DECEL_Y[i], tc_td_decel);
			//
			iface.observeDataRef(TD_ID[i], tc_td_decel);
			iface.observeDataRef(TD_SHOW[i], tc_td_decel);
			iface.observeDataRef(TD_X[i], tc_td_decel);
			iface.observeDataRef(TD_Y[i], tc_td_decel);
		}
		

		
		iface.includeDataRef(FIX_DIST_0);
		iface.includeDataRef(FIX_DIST_1);
		iface.includeDataRef(FIX_DIST_2);
		iface.includeDataRef(FIX_RAD_DIST_0);
		iface.includeDataRef(FIX_RAD_DIST_1);
		iface.includeDataRef(FIX_RAD_DIST_2);
		iface.observeDataRef(FIX_DIST_0, fixes);
		iface.observeDataRef(FIX_DIST_1, fixes);
		iface.observeDataRef(FIX_DIST_2, fixes);
		iface.observeDataRef(FIX_RAD_DIST_0, fixes);
		iface.observeDataRef(FIX_RAD_DIST_1, fixes);
		iface.observeDataRef(FIX_RAD_DIST_2, fixes);
		
		iface.includeDataRef(FIX_ID0);
		iface.includeDataRef(FIX_ID1);
		iface.includeDataRef(FIX_ID2);
		iface.includeDataRef(FIX_ID3);
		iface.includeDataRef(FIX_ID4);
		iface.includeDataRef(FIX_FO_ID0);
		iface.includeDataRef(FIX_FO_ID1);
		iface.includeDataRef(FIX_FO_ID2);
		iface.includeDataRef(FIX_FO_ID3);
		iface.includeDataRef(FIX_FO_ID4);
		
		iface.observeDataRef(FIX_ID0, fixes);
		iface.observeDataRef(FIX_ID1, fixes);
		iface.observeDataRef(FIX_ID2, fixes);
		iface.observeDataRef(FIX_ID3, fixes);
		iface.observeDataRef(FIX_ID4, fixes);
		iface.observeDataRef(FIX_FO_ID0, fixes);
		iface.observeDataRef(FIX_FO_ID1, fixes);
		iface.observeDataRef(FIX_FO_ID2, fixes);
		iface.observeDataRef(FIX_FO_ID3, fixes);
		iface.observeDataRef(FIX_FO_ID4, fixes);
		
		for(int i = 0; i < 2; i++) {

			iface.includeDataRef(FIX_ID_SHOW[i]);
			iface.observeDataRef(FIX_ID_SHOW[i], fixes);
		}
		



		iface.includeDataRef(ILS_X, 0.01f);
		iface.includeDataRef(ILS_Y, 0.01f);
		iface.includeDataRef(ILS_X0, 0.01f);
		iface.includeDataRef(ILS_Y0, 0.01f);
		//
		iface.includeDataRef(ILS_FO_X, 0.01f);
		iface.includeDataRef(ILS_FO_Y, 0.01f);
		iface.includeDataRef(ILS_FO_X0, 0.01f);
		iface.includeDataRef(ILS_FO_Y0, 0.01f);
		//
		iface.includeDataRef(ILS_RUNWAY);
		iface.includeDataRef(ILS_RUNWAY0);

		
		iface.observeDataRef(ILS_X, runways);
		iface.observeDataRef(ILS_Y, runways);
		iface.observeDataRef(ILS_X0, runways);
		iface.observeDataRef(ILS_Y0, runways);
		iface.observeDataRef(ILS_FO_X, runways);
		iface.observeDataRef(ILS_FO_Y, runways);
		iface.observeDataRef(ILS_FO_X0, runways);
		iface.observeDataRef(ILS_FO_Y0, runways);
		iface.observeDataRef(ILS_RUNWAY, runways);
		iface.observeDataRef(ILS_RUNWAY0, runways);
		
	}


	@Override
	public void excludeDrefs() {
		
		//irs
		for(String dref : drefs) {
			iface.excludeDataRef(dref);
			iface.unObserveDataRef(dref, irs);
		}	
		
		for(int i = 0; i < 2; i++) {
			iface.excludeDataRef(TC_ID[i]);
			iface.excludeDataRef(TC_SHOW[i]);
			iface.excludeDataRef(TC_X[i]);
			iface.excludeDataRef(TC_Y[i]);
			//
			iface.excludeDataRef(DECEL_ID[i]);
			iface.excludeDataRef(DECEL_SHOW[i]);
			iface.excludeDataRef(DECEL_X[i]);
			iface.excludeDataRef(DECEL_Y[i]);
			//
			iface.excludeDataRef(TD_ID[i]);
			iface.excludeDataRef(TD_SHOW[i]);
			iface.excludeDataRef(TD_X[i]);
			iface.excludeDataRef(TD_Y[i]);
			//
			iface.unObserveDataRef(TC_ID[i], tc_td_decel);
			iface.unObserveDataRef(TC_SHOW[i], tc_td_decel);
			iface.unObserveDataRef(TC_X[i], tc_td_decel);
			iface.unObserveDataRef(TC_Y[i], tc_td_decel);
			//
			iface.unObserveDataRef(DECEL_ID[i], tc_td_decel);
			iface.unObserveDataRef(DECEL_SHOW[i], tc_td_decel);
			iface.unObserveDataRef(DECEL_X[i], tc_td_decel);
			iface.unObserveDataRef(DECEL_Y[i], tc_td_decel);
			//
			iface.unObserveDataRef(TD_ID[i], tc_td_decel);
			iface.unObserveDataRef(TD_SHOW[i], tc_td_decel);
			iface.unObserveDataRef(TD_X[i], tc_td_decel);
			iface.unObserveDataRef(TD_Y[i], tc_td_decel);
		}
		

		
		iface.excludeDataRef(FIX_DIST_0);
		iface.excludeDataRef(FIX_DIST_1);
		iface.excludeDataRef(FIX_DIST_2);
		iface.excludeDataRef(FIX_RAD_DIST_0);
		iface.excludeDataRef(FIX_RAD_DIST_1);
		iface.excludeDataRef(FIX_RAD_DIST_2);
		iface.unObserveDataRef(FIX_DIST_0, fixes);
		iface.unObserveDataRef(FIX_DIST_1, fixes);
		iface.unObserveDataRef(FIX_DIST_2, fixes);
		iface.unObserveDataRef(FIX_RAD_DIST_0, fixes);
		iface.unObserveDataRef(FIX_RAD_DIST_1, fixes);
		iface.unObserveDataRef(FIX_RAD_DIST_2, fixes);
		
		iface.excludeDataRef(FIX_ID0);
		iface.excludeDataRef(FIX_ID1);
		iface.excludeDataRef(FIX_ID2);
		iface.excludeDataRef(FIX_ID3);
		iface.excludeDataRef(FIX_ID4);
		iface.excludeDataRef(FIX_FO_ID0);
		iface.excludeDataRef(FIX_FO_ID1);
		iface.excludeDataRef(FIX_FO_ID2);
		iface.excludeDataRef(FIX_FO_ID3);
		iface.excludeDataRef(FIX_FO_ID4);
		
		iface.unObserveDataRef(FIX_ID0, fixes);
		iface.unObserveDataRef(FIX_ID1, fixes);
		iface.unObserveDataRef(FIX_ID2, fixes);
		iface.unObserveDataRef(FIX_ID3, fixes);
		iface.unObserveDataRef(FIX_ID4, fixes);
		iface.unObserveDataRef(FIX_FO_ID0, fixes);
		iface.unObserveDataRef(FIX_FO_ID1, fixes);
		iface.unObserveDataRef(FIX_FO_ID2, fixes);
		iface.unObserveDataRef(FIX_FO_ID3, fixes);
		iface.unObserveDataRef(FIX_FO_ID4, fixes);
		
		for(int i = 0; i < 2; i++) {

			iface.excludeDataRef(FIX_ID_SHOW[i]);
			iface.unObserveDataRef(FIX_ID_SHOW[i], fixes);
		}
		



		iface.excludeDataRef(ILS_X);
		iface.excludeDataRef(ILS_Y);
		iface.excludeDataRef(ILS_X0);
		iface.excludeDataRef(ILS_Y0);
		//
		iface.excludeDataRef(ILS_FO_X);
		iface.excludeDataRef(ILS_FO_Y);
		iface.excludeDataRef(ILS_FO_X0);
		iface.excludeDataRef(ILS_FO_Y0);
		//
		iface.excludeDataRef(ILS_RUNWAY);
		iface.excludeDataRef(ILS_RUNWAY0);

		
		iface.unObserveDataRef(ILS_X, runways);
		iface.unObserveDataRef(ILS_Y, runways);
		iface.unObserveDataRef(ILS_X0, runways);
		iface.unObserveDataRef(ILS_Y0, runways);
		iface.unObserveDataRef(ILS_FO_X, runways);
		iface.unObserveDataRef(ILS_FO_Y, runways);
		iface.unObserveDataRef(ILS_FO_X0, runways);
		iface.unObserveDataRef(ILS_FO_Y0, runways);
		iface.unObserveDataRef(ILS_RUNWAY, runways);
		iface.unObserveDataRef(ILS_RUNWAY0, runways);
		
	}

}
