/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zhsi.displays.nd;

import javax.swing.JPanel;

import org.andreels.zhsi.ModelFactory;

public class NDvariableRenderLoop implements Runnable {

	long lastLoopTime = System.nanoTime();
	final int TARGET_FPS = 30;
	final long OPTIMAL_TIME = 1000000000 / TARGET_FPS;
	private int lastFpsTime = 0;
	private int fps = 0;
	//private volatile boolean running = true;
	private volatile Thread renderThread;
	

	JPanel nd;
	String title;

	public NDvariableRenderLoop(ModelFactory model_factory, JPanel nd, String title) {
		this.nd = nd;
		this.title = title;
		renderThread = new Thread(this);
		renderThread.start();
	}
	
	public void terminate() {
        //running = false;
        renderThread = null;
    }

	@Override
	public void run() {
		Thread thisThread = Thread.currentThread();
		while (renderThread == thisThread)
		{
			
			long now = System.nanoTime();
			long updateLength = now - lastLoopTime;
			lastLoopTime = now;
			double delta = updateLength / ((double)OPTIMAL_TIME);

			lastFpsTime += updateLength;
			fps++;

			if (lastFpsTime >= 1000000000)
			{
				//System.out.println("(" + title + " FPS: "+fps+")");	
				lastFpsTime = 0;
				fps = 0;
			}
			if(this.nd.isShowing()) {
				this.nd.repaint();
			}
			

			try {
				Thread.sleep(Math.abs(lastLoopTime-System.nanoTime() + OPTIMAL_TIME)/1000000 );
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
}
